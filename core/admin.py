from django.contrib import admin

from .models import *

# Register your models here.

@admin.register(Tecnico)
class TecnicoAdmin(admin.ModelAdmin):
    list_display = [
        "Numero",
        "NomPila",
        "PrimApell",
        "SegApell",
        "NumTel",
        "Correo",
        "Usuario",
        "Contraseña",
    ]

@admin.register(Servicios)
class ServicioAdmin(admin.ModelAdmin):
    list_display = [
        "Numero",
        "Descripcion",
        "Maquina",
        "Cita",
    ]

@admin.register(Marcas)
class MarcasAdmin(admin.ModelAdmin):
    list_display = [
        "Codigo",
        "Nombre",
    ]

@admin.register(Modelos)
class ModelosAdmin(admin.ModelAdmin):
    list_display = [
        "Codigo",
        "Nombre",
        "Año",
        "Marca",
    ]

@admin.register(TipoMaquinas)
class TmaquinasAdmin(admin.ModelAdmin):
    list_display = [
        "Codigo",
        "Nombre",
        "Descripcion",
    ]

@admin.register(Maquinas)
class MaquinasAdmin(admin.ModelAdmin):
    list_display = [
        "Codigo",
        "Descripcion",
        "Tipo_Maquina",
        "Marca",
        "Modelo",
    ]

@admin.register(Cliente)
class CleintesAdmin(admin.ModelAdmin):
    list_display = [
        "Numero",
        "Nombre",
        "NumTel",
        "ContNomPila",
        "ContPrimApell",
        "ContSegApell",
        "Usuario",
        "Contraseña",
    ]

@admin.register(Citas)
class CitasAdmin(admin.ModelAdmin):
    list_display = [
        "Numero",
        "Fecha",
        "Hora",
        "Tecnico",
        "Cliente",
    ]