from django import forms
from .models import *

from django import forms
from .models import Tecnico

## T E C N I C O S 

class TecnicoForm(forms.ModelForm):
    class Meta:
        model = Tecnico
        fields = "__all__"
        widgets = {
            "Numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número de empleado"}),
            "NomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre pila del técnico"}),
            "PrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido del técnico"}),
            "SegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido del técnico"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número telefónico del técnico"}),
            "Correo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Correo electrónico del técnico"}),
            "Usuario": forms.TextInput(attrs={"class": "form-control", "placeholder": "Usuario del técnico"}),
            "Contraseña": forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Contraseña del técnico"}),
        }

class UpdateTecnicoForm(forms.ModelForm):
    class Meta:
        model = Tecnico
        fields = "__all__"
        widgets = {
            
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número telefónico del técnico"}),
            "Correo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Correo electrónico del técnico"}),
            "Usuario": forms.TextInput(attrs={"class": "form-control", "placeholder": "Usuario del técnico"}),
            "Contraseña": forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Contraseña del técnico"}),
        }

## C L I E N T E S

class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = "__all__"
        widgets = {
            "Numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número del cliente"}),
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del cliente si es moral"}),
            "NumTel": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número telefónico del cliente"}),
            "ContNomPila": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre pila del contacto"}),
            "ContPrimApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Primer apellido del contacto"}),
            "ContSegApell": forms.TextInput(attrs={"class": "form-control", "placeholder": "Segundo apellido del contacto"}),
            "Usuario": forms.TextInput(attrs={"class": "form-control", "placeholder": "Usuario del contacto"}),
            "Contraseña": forms.PasswordInput(attrs={"class": "form-control", "placeholder": "Contraseña del contacto"}),
        }

## T I P O   D E   M A Q U I NA

class TipoMaquinasForm(forms.ModelForm):
    class Meta:
        model = TipoMaquinas
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del tipo de maquina"}),
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del tipo de maquina"}),
            "Descipcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Deescipcion del tipo de maquina"}),
        }


## C A M B I O S

class CambiosForm(forms.ModelForm):
    class Meta:
        model = Cambios
        fields = "__all__"

## M A R C A S

class MarcasForm(forms.ModelForm):
    class Meta:
        model = Marcas
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo de la marca"}),
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre de marca"}),
        }

## M O D E L O S

class ModelosForm(forms.ModelForm):
    class Meta:
        model = Modelos
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del modelo"}),
            "Nombre": forms.TextInput(attrs={"class": "form-control", "placeholder": "Nombre del modelo"}),
            "Año": forms.TextInput(attrs={"class": "form-control", "placeholder": "Año del modelo"}),
            "Marca": forms.TextInput(attrs={"class": "form-control", "placeholder": "Marca del modelo"}),
        }

## M A Q U I N A S

class MaquinasForm(forms.ModelForm):
    class Meta:
        model = Maquinas
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del la maquina"}),
            "Descipción": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de la maquina"}),
            "Tipo_maquina": forms.TextInput(attrs={"class": "form-control", "placeholder": "Tipo de maquina"}),
            "Marca": forms.TextInput(attrs={"class": "form-control", "placeholder": "Marca de la maquina"}),
            "Modelo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Modelo de la maquina"}),
        }

class UpdateMaquinasForm(forms.ModelForm):
    class Meta:
        model = Maquinas
        fields = "__all__"
        widgets = {
            "Codigo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Codigo del la maquina"}),
            "Descipción": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de la maquina"}),
            "Tipo_maquina": forms.TextInput(attrs={"class": "form-control", "placeholder": "Tipo de maquina"}),
            "Marca": forms.TextInput(attrs={"class": "form-control", "placeholder": "Marca de la maquina"}),
            "Modelo": forms.TextInput(attrs={"class": "form-control", "placeholder": "Modelo de la maquina"}),
        }

## C I T A S

class CitasForm(forms.ModelForm):
    class Meta:
        model = Citas
        fields = "__all__"
        widgets = {
            "Numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Numero de la cita"}),
            "Fecha": forms.TextInput(attrs={"class": "form-control", "placeholder": "Fecha de la cita"}),
            "Hora": forms.TextInput(attrs={"class": "form-control", "placeholder": "Hora de la cita"}),
            "Tecnico": forms.TextInput(attrs={"class": "form-control", "placeholder": "Tecnico de la cita"}),
            "Cliente": forms.TextInput(attrs={"class": "form-control", "placeholder": "Cliente de la cita"}),
        }

## S E R V I C I O S

class ServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicios
        fields = "__all__"
        widgets = {
            "Numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número Servicio"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de servicio"}),
            "Maquina": forms.TextInput(attrs={"class": "form-control", "placeholder": "Mquina a la que se le hizo servicio"}),
            "Cita": forms.TextInput(attrs={"class": "form-control", "placeholder": "Servicio de la cita que se hizo"}),
        }
    
class UpdateServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicios
        fields = "__all__"
        widgets = {
            "Numero": forms.TextInput(attrs={"class": "form-control", "placeholder": "Número Servicio"}),
            "Descripcion": forms.TextInput(attrs={"class": "form-control", "placeholder": "Descripcion de servicio"}),
            "Maquina": forms.TextInput(attrs={"class": "form-control", "placeholder": "Mquina a la que se le hizo servicio"}),
            "Cita": forms.TextInput(attrs={"class": "form-control", "placeholder": "Servicio de la cita que se hizo"}),
        }

## S E R V I C I O S   C A M B I O S

class ServCambioForm(forms.ModelForm):
    class Meta:
        model = ServCambio
        fields = "__all__"