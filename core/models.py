from django.db import models

from django.contrib.auth.models import User

# Create your models here.

class Tecnico(models.Model):
    Numero = models.IntegerField(primary_key=True, unique=True)
    NomPila = models.CharField(max_length=30, null=False)
    PrimApell = models.CharField(max_length=30, null=False)
    SegApell = models.CharField(max_length=30, blank=True)
    NumTel = models.CharField(max_length=15, null=False)
    Correo = models.CharField(max_length=60, blank=True)
    Usuario = models.CharField(max_length=60, blank=True)
    Contraseña = models.CharField(max_length=20, blank=True)

class Cliente(models.Model):
    Numero = models.IntegerField(primary_key=True, unique=True)
    Nombre = models.CharField(max_length=50, null=False)
    NumTel = models.CharField(max_length=15, null=False)
    ContNomPila = models.CharField(max_length=30, null=False)
    ContPrimApell = models.CharField(max_length=30, null=False)
    ContSegApell = models.CharField(max_length=30, blank=True)
    Usuario = models.CharField(max_length=60, blank=True)
    Contraseña = models.CharField(max_length=20, blank=True)

class TipoMaquinas(models.Model):
    Codigo = models.CharField(max_length=5, primary_key=True, unique=True)
    Nombre = models.CharField(max_length=30, null=False)
    Descripcion = models.CharField(max_length=100, null=False)

class Cambios(models.Model):
    Codigo = models.CharField(max_length=5, primary_key=True, unique=True)
    Descripcion = models.CharField(max_length=100, null=False)

class Marcas(models.Model):
    Codigo = models.CharField(max_length=5, primary_key=True, unique=True)
    Nombre = models.CharField(max_length=30, null=False)

class Modelos(models.Model):
    Codigo = models.CharField(max_length=5, primary_key=True, unique=True)
    Nombre = models.CharField(max_length=30, null=False)
    Año = models.IntegerField(null=True)
    Marca = models.ForeignKey(Marcas, on_delete=models.CASCADE)

class Maquinas(models.Model):
    Codigo = models.CharField(max_length=5, primary_key=True, unique=True)
    Descripcion = models.CharField(max_length=100, blank=True)
    Tipo_Maquina = models.ForeignKey(TipoMaquinas, on_delete=models.CASCADE)
    Marca = models.ForeignKey(Marcas, on_delete=models.CASCADE)
    Modelo = models.ForeignKey(Modelos, on_delete=models.CASCADE)

class Citas(models.Model):
    Numero = models.IntegerField(primary_key=True, unique=True)
    Fecha = models.DateField(null=False)
    Hora = models.CharField(max_length=10, null=False)
    Tecnico = models.ForeignKey(Tecnico, on_delete=models.CASCADE)
    Cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)

class Servicios(models.Model):
    Numero = models.IntegerField(primary_key=True, unique=True)
    Descripcion = models.CharField(max_length=200, null=False)
    Maquina = models.ForeignKey(Maquinas, on_delete=models.CASCADE)
    Cita = models.ForeignKey(Citas, on_delete=models.CASCADE)

class ServCambio(models.Model):
    Servicio = models.ForeignKey(Servicios, on_delete=models.CASCADE)
    Cambio = models.ForeignKey(Cambios, on_delete=models.CASCADE)
    Cantidad = models.IntegerField(null=False)