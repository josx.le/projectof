from django.shortcuts import render

from django.views import generic

from django.urls import reverse_lazy
from .models import *
from .forms import *


# Create your views here.

## T E C N I C O S

class List1(generic.View):
    template_name = "core/list1.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Tecnico.objects.all()
        self.context = {
            "Tecnico": queryset
        }
        return render(request, self.template_name,self.context)


class DetailTecnico(generic.DetailView):
    template_name = "core/detail1.html"
    model = Tecnico

class CreateTecnico(generic.CreateView):
    template_name = "core/create1.html"
    model = Tecnico
    form_class = TecnicoForm
    success_url = reverse_lazy("core:list1")

class UpdateTecnicos(generic.UpdateView):
    template_name = "core/Update1.html"
    model = Tecnico
    form_class = UpdateTecnicoForm
    success_url = reverse_lazy("core:list1")

class DeleteTecnico(generic.DeleteView):
    template_name = "core/delete1.html"
    model = Tecnico
    success_url = reverse_lazy("core:list1")

## S E R V I C I O S

class List2(generic.View):
    template_name = "core/list2.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Servicios.objects.all()
        self.context = {
            "Servicios": queryset
        }
        return render(request, self.template_name,self.context)
    
class DetailServicio(generic.DetailView):
    template_name = "core/detail2.html"
    model = Servicios

class CreateServicio(generic.CreateView):
    template_name = "core/create2.html"
    model = Servicios
    form_class = ServiciosForm
    success_url = reverse_lazy("core:list2")

class UpdateServicios(generic.UpdateView):
    template_name = "core/Update2.html"
    model = Servicios
    form_class = UpdateServiciosForm
    success_url = reverse_lazy("core:list2")

class DeleteServicios(generic.DeleteView):
    template_name = "core/delete2.html"
    model = Servicios
    success_url = reverse_lazy("core:list2")

##  M A Q U I N A S

class List3(generic.View):
    template_name = "core/list3.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Maquinas.objects.all()
        self.context = {
            "Maquinas": queryset
        }
        return render(request, self.template_name,self.context)

class DetailMaquinas(generic.DetailView):
    template_name = "core/detail3.html"
    model = Maquinas

class CreateMaquina(generic.CreateView):
    template_name = "core/create3.html"
    model = Maquinas
    form_class = MaquinasForm
    success_url = reverse_lazy("core:list3")

class UpdateMaquina(generic.UpdateView):
    template_name = "core/Update3.html"
    model = Maquinas
    form_class = UpdateMaquinasForm
    success_url = reverse_lazy("core:list3")

class DeleteMaquina(generic.DeleteView):
    template_name = "core/delete3.html"
    model = Maquinas
    success_url = reverse_lazy("core:list3")